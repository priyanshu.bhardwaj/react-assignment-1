import React from "react";
import ButtonComponent from "./ButtonComponent";

const Buttons = ({
  isActive,
  setIsActive,
  startTime,
  stopTime,
  createLap,
  reset,
}) => {
  return (
    <div className="flex gap-5 mt-5">
      <ButtonComponent
        title={isActive ? "Lap" : "Reset"}
        createLap={createLap}
        reset={reset}
      />
      <ButtonComponent
        title={isActive ? "Stop" : "Start"}
        startTime={startTime}
        stopTime={stopTime}
      />
    </div>
  );
};

export default Buttons;
