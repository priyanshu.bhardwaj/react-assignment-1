import React from "react";

const ButtonComponent = ({ title, startTime, stopTime, createLap, reset }) => {
  const handleClick = () => {
    switch (title) {
      case "Start":
        startTime();
        break;

      case "Stop":
        stopTime();
        break;

      case "Lap":
        createLap();
        break;

      case "Reset":
        reset();
        break;

      default:
        break;
    }
  };

  return (
    <div className="w-20 flex flex-col items-center justify-center text-gray-500">
      <button onClick={handleClick}>{title}</button>
    </div>
  );
};

export default ButtonComponent;
