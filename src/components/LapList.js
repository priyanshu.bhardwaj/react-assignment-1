import React, { useEffect, useState } from "react";

const LapList = ({ laps }) => {
  const [arr, setArr] = useState([]);
  const [minEle, setMinEle] = useState(null);
  const [maxEle, setMaxEle] = useState(null);

  useEffect(() => {
    let newArr = [];
    for (let i = 0; i < laps.length; i++) {
      if (i === laps.length - 1) {
        newArr.push(laps[i]);
      } else {
        newArr.push(laps[i] - laps[i + 1]);
      }
    }
    setArr(newArr);
    setMinEle(Math.min(...newArr));
    setMaxEle(Math.max(...newArr));
  }, [laps]);

  return (
    <div className="w-fit flex flex-col gap-3 mt-16">
      {arr.map((item, index) => {
        return (
          <div
            className="w-72 m-auto flex justify-between font-medium"
            key={index}
          >
            <div>Lap {arr.length - index}</div>
            <div
              className={
                arr.length > 2 && minEle === item
                  ? "text-red-500"
                  : arr.length > 2 && maxEle === item
                  ? "text-green-500"
                  : ""
              }
            >
              <span>{("0" + Math.floor((item / 60000) % 60)).slice(-2)}:</span>
              <span>{("0" + Math.floor((item / 1000) % 60)).slice(-2)}:</span>
              <span>{("0" + ((item / 10) % 1000)).slice(-2)}</span>
            </div>
          </div>
        );
      })}
    </div>
  );
};

export default LapList;
