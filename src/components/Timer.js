import React from "react";

const Timer = ({ time }) => {
  return (
    <div className="text-6xl font-medium flex items-center gap-2">
      <div className="w-20 flex justify-center">
        {("0" + Math.floor((time / 60000) % 60)).slice(-2)}
      </div>
      <div>:</div>
      <div className="w-20 flex justify-center">
        {("0" + Math.floor((time / 1000) % 60)).slice(-2)}
      </div>
      <div>:</div>
      <div className="w-10 flex justify-center">
        {("0" + ((time / 10) % 100)).slice(-2, -1)[0]}
      </div>
    </div>
  );
};

export default Timer;
