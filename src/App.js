import { useEffect, useState } from "react";
import "./App.css";
import Buttons from "./components/Button/Buttons";
import LapList from "./components/LapList";
import Timer from "./components/Timer";

function App() {
  const [isActive, setIsActive] = useState(false);
  const [time, setTime] = useState(0);
  const [laps, setLaps] = useState([]);

  useEffect(() => {
    let interval = null;
    if (isActive) {
      interval = setInterval(() => {
        setTime((prev) => prev + 10);
      }, 10);
    }
    return () => clearInterval(interval);
  }, [isActive]);

  const startTime = () => {
    setIsActive(true);
  };

  const stopTime = () => {
    setIsActive(false);
  };

  const createLap = () => {
    setLaps((prev) => [time, ...prev]);
  };

  const reset = () => {
    setTime(0);
    setLaps([]);
  };

  return (
    <div className="bg-indigo-50 min-h-screen w-full flex flex-col items-center py-20">
      <Timer time={time} />
      <Buttons
        isActive={isActive}
        setIsActive={setIsActive}
        startTime={startTime}
        stopTime={stopTime}
        createLap={createLap}
        reset={reset}
      />
      <LapList laps={laps} />
    </div>
  );
}

export default App;
